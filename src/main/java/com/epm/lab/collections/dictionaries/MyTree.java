package com.epm.lab.collections.dictionaries;

public class MyTree {
    private Node root;

    public MyTree() {
        root = null;
    }

    public Node find(int key) {
        Node current = root;
        while (current.keyData != key) {
            if (key < current.keyData) {
                current = current.leftNode;
            } else {
                current = current.rightNode;
            }
            if (current == null) {
                return null;
            }
        }
        return current;
    }

    public void insert(int kd, double vd) {
        Node newNode = new Node();
        newNode.keyData = kd;
        newNode.valueData = vd;
        if (root == null)
            root = newNode;
        else {
            Node current = root;
            Node parent;
            while (true) {
                parent = current;
                if (kd < current.keyData) {
                    current = current.leftNode;
                    if (current == null) {
                        parent.leftNode = newNode;
                        return;
                    }
                } else {
                    current = current.rightNode;
                    if (current == null) {
                        parent.rightNode = newNode;
                        return;
                    }
                }
            }
        }
    }
}
