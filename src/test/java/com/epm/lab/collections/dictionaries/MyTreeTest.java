package com.epm.lab.collections.dictionaries;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyTreeTest {

    @Test
    void find() {
        //GIVEN
        MyTree myTreeTest = new MyTree();
        //WHEN
        myTreeTest.insert(25, 99.9);
        Node nodeFoundTest = myTreeTest.find(25);
        //THEN
        assertEquals(25, 25);
    }

    @Test
    void insert() {
        //GIVEN
        MyTree myTreeTest = new MyTree();
        //WHEN
        myTreeTest.insert(25, 99.9);
        //THEN
        assertEquals(25, 25);
    }
}