package com.epm.lab.collections.dictionaries;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyMapTest {

    @Test
    void get() {
        //GIVEN
        MyMap<String, String> getTest = new MyMap<>(1);
        //WHEN
        getTest.put("test", "object");
        getTest.get("test");
        //THEN
        assertEquals("test", "test");
    }

    @Test
    void put() {
        //GIVEN
        MyMap<String, String> getTest = new MyMap<>(1);
        //WHEN
        getTest.put("test", "object");
        //THEN
        assertEquals("test", "test");
    }

    @Test
    void size() {
        //GIVEN
        MyMap<String, String> getTest = new MyMap<>(1);
        //WHEN
        getTest.put("test", "object");
        //THEN
        assertEquals(1, getTest.size());
    }
}